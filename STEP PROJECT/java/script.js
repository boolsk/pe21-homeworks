+function () {
    document.querySelector(".tabs-title").classList.add("active");
    document.querySelector(".tab-text").classList.add("active");

    function selectTab(event) {
        let target = event.target.dataset.target;
        document.querySelectorAll(".tabs-title").forEach(el => {
            el.classList.remove("active");
            event.target.classList.add("active");
            document.querySelector("." + target).classList.add("active");
        });
        document.querySelectorAll(".tab-text").forEach(el => {
            el.classList.remove("active");
            event.target.classList.add("active");
            document.querySelector("." + target).classList.add("active");
        });
    }

    document.querySelectorAll(".tabs-title").forEach(el => {
        el.addEventListener("click", selectTab)
    })
}();


function filterForPic() {
    let btnAmazLoad = document.getElementById("amazing-btn-load");
    let tabsAmazWrk = document.getElementsByClassName("tabs-title-our-amazing-work");
    const filterBoxPic = document.querySelectorAll(".boxPicture");
    document.querySelector(".tabs-our-amazing-work").addEventListener("click", event => {
        if (event.target.tagName !== "LI") return false;
        let clickOnMenu = event.target;
        for (let i = 0; i < tabsAmazWrk.length; i++) {
            tabsAmazWrk[i].classList.remove("active-amaz-menu");
        }
        clickOnMenu.classList.add("active-amaz-menu");
        let filterByClass = event.target.dataset["filtertarget"];
        filterBoxPic.forEach(element => {
            element.classList.remove("hide");
            element.classList.remove("hide-pict-our-amazing");
            if (!element.classList.contains(filterByClass) && filterByClass !== "all") {
                element.classList.add("hide");
                btnAmazLoad.classList.add("hide");
            }
        })
    });
}

filterForPic();

document.getElementById("amazing-btn-load").addEventListener("click", loadMore);

function loadMore() {
    let btnAmazLoad = document.getElementById("amazing-btn-load");
    const boxPictureArr = document.querySelectorAll(".boxPicture");
    for (let i = 0; i < boxPictureArr.length; i++) {
        boxPictureArr[i].classList.remove("hide-pict-our-amazing");
        btnAmazLoad.classList.add("sec4-green-btn:active");
        btnAmazLoad.classList.add("hide");
    }
}


let slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function prevSlides(n) {
    showSlides(slideIndex += n);
}

function showSlides(n) {
    let i;
    let slides = document.getElementsByClassName("mySlides");
    let dots = document.getElementsByClassName("dot");
    if (n > slides.length) {
        slideIndex = 1;
    }
    if (n < 1) {
        slideIndex = slides.length;
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    dots[slideIndex - 2 & slideIndex + 2].classList.remove("active-for-slider");
    for (let j = 0; j < dots.length; j++) {
        dots[j].classList.remove("active-for-slider");
        slides[slideIndex - 1].style.display = "flex";
        dots[slideIndex - 1].classList.add("active-for-slider");
    }
}