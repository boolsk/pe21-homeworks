const books = [
    {
        author: "Скотт Бэккер",
        name: "Тьма, что приходит прежде",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Воин-пророк",
    },
    {
        name: "Тысячекратная мысль",
        price: 70
    },
    {
        author: "Скотт Бэккер",
        name: "Нечестивый Консульт",
        price: 70
    },
    {
        author: "Дарья Донцова",
        name: "Детектив на диете",
        price: 40
    },
    {
        author: "Дарья Донцова",
        name: "Дед Снегур и Морозочка",
    }
];

let newArrBooks = [...books];

function myBooks() {
    const bookList = document.getElementById("root");

    newArrBooks.forEach((el) => {
        if (el.author === undefined) {
            console.log("ОШИБКА!! Не указан автор!")
        }
        else if (el.name === undefined) {
            console.log("ОШИБКА!! Не указано название Книги!")
        }
        else if (el.price === undefined) {
            console.log("ОШИБКА!! Не указана Цена!")
        }
    });

    books.forEach((elem) => {
        const bookWrapper = document.createElement("ul");
        bookWrapper.style.border = '2px solid black';
        const bookAuthor = document.createElement("p");
        const bookName = document.createElement("p");
        const bookPrice = document.createElement("p");
        bookWrapper.append(bookAuthor, bookName, bookPrice);
        bookList.append(bookWrapper);
        bookAuthor.append("Автор: " + elem.author);
        bookName.append("Название книги: " + elem.name);
        bookPrice.append("Цена: " + elem.price);
    });
}

myBooks();





