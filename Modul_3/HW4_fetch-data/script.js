
// Задание
// Получить список фильмов серии Звездные войны, и вывести на экран список персонажей для каждого из них.
//
//     Технические требования:
//
//     Отправить AJAX запрос по адресу https://swapi.dev/api/films и получить список всех фильмов серии Звездные войны
//
//     Для каждого фильма получить с сервера список персонажей, которые были показаны в данном фильме. Список персонажей можно получить из свойства characters.
//     Как только с сервера будет получена информация о фильмах, сразу же вывести список всех фильмов на экран. Необходимо указать номер эпизода, название фильма, а также короткое содержание (поля episode_id, title и opening_crawl).
// Как только с сервера будет получена информация о персонажах какого-либо фильма, вывести эту информацию на экран под названием фильма.
//


class FilmCard {
    constructor ({episode_id, title, opening_crawl}) {
        this.episode_id = episode_id;
        this.title = title;
        this.opening_crawl = opening_crawl;
        this.elements = {
            episode_id: document.createElement("p"),
            title: document.createElement("p"),
            opening_crawl: document.createElement("p"),
            characters: document.createElement("div"),
        }
    }
    render() {
        const filmWrapper = document.createElement("div");
        this.elements.episode_id.textContent = this.episode_id;
        this.elements.title.textContent = this.title;
        this.elements.opening_crawl.textContent = this.opening_crawl;
        filmWrapper.append(this.elements.episode_id, this.elements.title, this.elements.opening_crawl, this.elements.characters);
        document.getElementById("film-wrapper").append(filmWrapper)
    }
    setCharacters(characters) {
        const persons = characters.map( pers => {
            const newField = document.createElement("p");
            newField.textContent = pers.name;
            return newField
        })
        this.elements.characters.append(...persons);
    }
}


fetch("https://swapi.dev/api/films")
    .then(r => r.json())
    .then(filmInfo => {
        const filmCards = filmInfo.results.map(film => {
            const filmCard = new FilmCard(film)
            const charactersPromises = film.characters.map(url => fetch(url).then(r => r.json()));
            Promise.all(charactersPromises)
                .then(characters => filmCard.setCharacters(characters))
            return filmCard
        })
        filmCards.forEach(filmCard => filmCard.render())
    })
