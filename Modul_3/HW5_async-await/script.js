// Написать программу "Я тебя по айпи вычислю"
//
// Технические требования:
//
//     Создать простую HTML страницу с кнопкой Вычислить по IP.
//     По нажатию на кнопку - отправить AJAX запрос по адресу https://api.ipify.org/?format=json, получить оттуда IP адрес клиента.
//     Узнав IP адрес, отправить запрос на сервис https://ip-api.com/ и получить информацию о физическом адресе.
//     Под кнопкой вывести на страницу информацию, полученную из последнего запроса - континент, страна, регион, город, район города.
//     Все запросы на сервер необходимо выполнить с помощью async await.

const ipBtn = document.getElementById("myIpButton");

async function getMyIP() {
   const myIp = await fetch("https://api.ipify.org/?format=json");
   return myIp.json();
}

async function getInfoByIp() {
    const myIp = await getMyIP();
    const myInfo = await fetch("http://ip-api.com/json/"+ myIp.ip + "?fields=continent,country,region,city,district");
    return myInfo.json();
}

class InfoByIp {
    constructor (continent,country,region,city,district) {
        this.continent = continent;
        this.country = country;
        this.region = region;
        this.city = city;
        this.district = district;
        this.elem = {
            continent: document.createElement("p"),
            country: document.createElement("p"),
            region: document.createElement("p"),
            city: document.createElement("p"),
            district: document.createElement("p"),
        }
    }
    render() {
        const wrapper = document.getElementById("ipInfoWrapper");
        this.elem.continent.textContent = "Continent: " + this.continent;
        this.elem.country.textContent = "Country: " + this.country;
        this.elem.region.textContent = "Region: " + this.region;
        this.elem.city.textContent = "City: " + this.city;
        this.elem.district.textContent = "District: " + this.district;
        wrapper.append(
            this.elem.continent,
            this.elem.country,
            this.elem.region,
            this.elem.city,
            this.elem.district
        );
        return wrapper;
    }
}

async function renderInfo () {
    const informationByIp =  await getInfoByIp();
    const userNewInfo = new InfoByIp(
        informationByIp.continent,
        informationByIp.country,
        informationByIp.region,
        informationByIp.city,
        informationByIp.district
    );
    userNewInfo.render()
}

ipBtn.addEventListener("click", () => {
    renderInfo ();
});

