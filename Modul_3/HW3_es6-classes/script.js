class Employee {
    constructor(name, age, salary) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get getName() {
        return this.name;
    }
    set setName(newName) {
        this.name = newName;
    }
    get getAge() {
        return this.age;
    }
    set setAge(newAge) {
        this.age = newAge;
    }
    get getSalary() {
        return this.salary;
    }
    set setSalary(newSalary) {
        this.salary = newSalary;
    }
}
const worker = new Employee( "Vlad", "30", 25000);
console.log(worker)
class Programmer extends Employee{
    constructor (name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
}
const programmer1 = new Programmer("Bob", 30, 20000, ["JS, PHP, scala"]);
const programmer2 = new Programmer("Peter", 35, 29000, ["JS, PHP"]);
const programmer3 = new Programmer("Vano", 48, 35000, ["JS"]);
console.log(programmer1, programmer2, programmer3);
