import React, {Component} from 'react';
import "./FavoriteStar.scss"
import { GrStar } from "react-icons/gr";

class FavoriteStar extends Component {

    render() {
        const {addToFavorites, prodId, favorites} = this.props
        const isFavorite = favorites.includes(prodId)
        return (
            <div className={"favorite-star " + (isFavorite && "favorite")}>
                <GrStar
                    onClick={() => addToFavorites(prodId)}
                    className="favorite-star__img"/>
            </div>
        );
    }
}

export default FavoriteStar;