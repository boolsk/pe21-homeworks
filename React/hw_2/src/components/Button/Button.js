import React, {Component} from 'react';
import "./Button.scss"

class Button extends Component {
    render() {
        const {text, backgroundColor, onClick} = this.props
        return (
            <button
                onClick={onClick}
                className="btn"
                style={{backgroundColor: backgroundColor}}>{text}</button>
        );
    }
}

export default Button;