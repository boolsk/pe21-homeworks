import React, {Component} from 'react';
import './App.css';
import ItemList from "./components/ItemList/ItemList";


class App extends Component {
    constructor(props) {
        super();
        this.state = {
            items: [],
            favorites: [],
            boughtProducts: []
        }
    }


    getBaza() {
        fetch("bazaProducts.json")
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                this.setState({
                    items: data
                });
            });
    }

    componentDidMount() {
        this.getBaza();
        if (localStorage.getItem("favorites")) {
            this.setState({favorites: JSON.parse(localStorage.getItem("favorites"))})
        }
        if (localStorage.getItem("boughtProducts")) {
            this.setState({boughtProducts: JSON.parse(localStorage.getItem("boughtProducts"))})
        }
    }

    addToFavorites = (id) => {

        const oldFavorites = this.state.favorites.map(id => id)
        console.log(oldFavorites)
        if (oldFavorites.includes(id)) {
            oldFavorites.splice(oldFavorites.indexOf(id), 1)
            this.setState({favorites: oldFavorites})
            console.log(this.state.favorites)
        } else {
            oldFavorites.push(id)
            this.setState({favorites: oldFavorites})
            console.log(this.state.favorites)
        }
        localStorage.setItem("favorites", JSON.stringify(oldFavorites))
    }

    addToCart = (id) => {
        const cartProducts = this.state.boughtProducts.map(id => id)
        console.log(cartProducts)
        if (cartProducts.includes(id)) {
            cartProducts.splice(cartProducts.indexOf(id), 1)
            this.setState({boughtProducts: cartProducts})
            console.log(this.state.boughtProducts)
        } else {
            cartProducts.push(id)
            this.setState({boughtProducts: cartProducts})
            console.log(this.state.boughtProducts)
        }
        localStorage.setItem("boughtProducts", JSON.stringify(cartProducts))
    }


    render() {
        return (
            <div className="App">
                <ItemList itemsProp={this.state.items}
                          addToFavorites={this.addToFavorites}
                          favorites={this.state.favorites}
                          addToCart={this.addToCart}
                          boughtProducts={this.state.boughtProducts}
                />
            </div>
        )
    }


}

export default App;
