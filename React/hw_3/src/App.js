import React, {useEffect, useState} from 'react';
import './App.css';
import Header from "./components/Header/Header";
import AppRoutes from "./routes/AppRoutes";

const App = () => {
    const [items, setItems] = useState([]);
    const [favorites, setFavorites] = useState(JSON.parse(localStorage.getItem("favorites")) || []);
    const [boughtProducts, setBoughtProducts] = useState(JSON.parse(localStorage.getItem("boughtProducts")) || []);
    useEffect(() => {
        getBaza();
    }, [])

    const getBaza = () => {
        fetch("bazaProducts.json")
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                setItems(data)
            });
    }


    const addToFavorites = (id) => {
        const oldFavorites = favorites.map(id => id)
        if (oldFavorites.includes(id)) {
            oldFavorites.splice(oldFavorites.indexOf(id), 1)
            setFavorites(
                oldFavorites
            )
            console.log(favorites)
        } else {
            oldFavorites.push(id)
            setFavorites(
                oldFavorites
            )
        }
        localStorage.setItem("favorites", JSON.stringify(oldFavorites))
    }
    //
    const addToCart = (id) => {
        const cartProducts = boughtProducts.map(id => id)
        console.log(cartProducts)
        if (cartProducts.includes(id)) {
            cartProducts.splice(cartProducts.indexOf(id), 1)
            setBoughtProducts(cartProducts)
            console.log(boughtProducts)
        } else {
            cartProducts.push(id)
            setBoughtProducts(cartProducts)

        }
        localStorage.setItem("boughtProducts", JSON.stringify(cartProducts))
    }


    return (
        <div>
            <>
                <Header/>
                <AppRoutes itemsProp={items}
                           addToFavorites={addToFavorites}
                           favorites={favorites}
                           addToCart={addToCart}
                           boughtProducts={boughtProducts}
                />
            </>

        </div>
    );
};

export default App;