import React from 'react';
import ItemList from "../components/ItemList/ItemList";
import {Route, Switch} from "react-router-dom";
import FavoritesItems from "../components/FavoritesItems/FavoritesItems";
import Cart from "../components/Cart/Cart";

const AppRoutes = ({itemsProp, addToFavorites, favorites, addToCart, boughtProducts}) => {
    return (
        <Switch>

            <Route exact path="/" render={
                () => (<ItemList
                        itemsProp={itemsProp}
                        addToFavorites={addToFavorites}
                        favorites={favorites}
                        addToCart={addToCart}
                        boughtProducts={boughtProducts}
                    />
                )
            }
            />

            <Route exact path="/favorites" render={
                () => (<FavoritesItems
                    itemsProp={itemsProp}
                    addToFavorites={addToFavorites}
                    favorites={favorites}
                    addToCart={addToCart}
                    boughtProducts={boughtProducts}
                />)

            }/>
            <Route exact path="/cart" render={
                () => (<Cart
                    itemsProp={itemsProp}
                    addToFavorites={addToFavorites}
                    favorites={favorites}
                    addToCart={addToCart}
                    boughtProducts={boughtProducts}
                />)

            }/>


        </Switch>
    );
};

export default AppRoutes;