import React from 'react';
import ProductItem from "../productItem/ProductItem";
import "./FavoritesItems.scss"

const FavoritesItems = ({boughtProducts, itemsProp, addToFavorites, favorites, addToCart}) => {
    return (
        <div className="favorites-items-wrapper">
            {itemsProp.map((item) => {
                return favorites.includes(item.id) && <ProductItem
                    prodId={item.id}
                    addToFavorites={addToFavorites}
                    title={item.title}
                    price={item.price}
                    pictureUrl={item.pictureUrl}
                    key={item.id}
                    favorites={favorites}
                    addToCart={addToCart}
                    cartItem={item}
                    boughtProducts={boughtProducts}/>
            })}
        </div>
    );
};

export default FavoritesItems;