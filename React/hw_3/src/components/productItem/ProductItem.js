import React, {useState} from 'react';
import Button from "../Button/Button";
import "./ProductItem.scss"
import FavoriteStar from "../FavoriteStar/FavoriteStar";
import Modal from "../Modal/Modal";


const ProductItem = ({title, price, pictureUrl, addToFavorites, prodId, favorites, addToCart, cartItem}) => {

    const [modal, setModal] = useState(false);
    const boughtProducts = localStorage.getItem("boughtProducts") || [];

    return (
        <div className="product-item">
            <div className="image-wrapper">
                <img
                    className="product-item__img"
                    src={pictureUrl}
                    alt="img"/>
            </div>
            <div className="product-item__description">
                <span>{title}</span>
            </div>
            <div className="product-item__description">
                <span>{price} грн</span>
            </div>
            <div className="product-item__btn">
                <Button
                    text= {boughtProducts.includes(prodId) ? "REMOVE" : "ADD"}
                    backgroundColor="red"
                    onClick={() => {
                        addToCart(cartItem.id)
                        setModal(true)
                    }}
                />
                {
                    modal &&
                    <Modal
                        text="Товар добавлен в Корзину"
                        bgColorWrapper="#968a8a"
                        bgColorHeader="#7a7171"
                        btnModCol="#7a7171"
                        closeModal={() => setModal(null)}
                    />
                }
                <FavoriteStar addToFavorites={addToFavorites} prodId={prodId} favorites={favorites}/>
            </div>
        </div>
    );
};

export default ProductItem;