import React from 'react';
import "./FavoriteStar.scss"
import { GrStar } from "react-icons/gr";


const FavoriteStar = ({addToFavorites, prodId, favorites}) => {
    const isFavorite = favorites.includes(prodId)
    return (
        <div className={"favorite-star " + (isFavorite && "favorite")}>
            <GrStar
                onClick={() => addToFavorites(prodId)}
                className="favorite-star__img"/>
        </div>
    )
}

export default FavoriteStar;