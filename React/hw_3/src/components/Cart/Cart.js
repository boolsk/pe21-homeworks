import React from 'react';
import ProductItem from "../productItem/ProductItem";
import "./Cart.scss"


const Cart = ({boughtProducts, itemsProp, addToFavorites, favorites, addToCart}) => {
    return (
        <div className="cart-wrapper">
            {itemsProp.map((item) => {
                return boughtProducts.includes(item.id) &&
                    <ProductItem
                    prodId={item.id}
                    addToFavorites={addToFavorites}
                    title={item.title}
                    price={item.price}
                    pictureUrl={item.pictureUrl}
                    key={item.id}
                    favorites={favorites}
                    addToCart={addToCart}
                    cartItem={item}
                    boughtProducts={boughtProducts}
                    />
            })}
        </div>
    );
};

export default Cart;