import React from 'react';
import "./Header.scss"
import {NavLink} from "react-router-dom";



const Header = () => {
    return (
        <div className='header'>
            <a href="/">MAIN</a>
            <div className="header__right-menu">
                <NavLink  to={"/favorites"} className="header__right-menu__favorites">Favorites</NavLink>
                <NavLink to={"/cart"} className="header__right-menu__cart">Cart</NavLink>
            </div>
        </div>
    );
};

export default Header;
