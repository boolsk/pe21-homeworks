import React, {Component} from 'react';
import "./Button.scss"

class Button extends Component {
    render() {
        const {text, onClick, backgroundColor} = this.props
        return (
            <button className="btn" onClick={onClick} style={{backgroundColor: backgroundColor}}>{text}</button>
        );
    }
}

export default Button;