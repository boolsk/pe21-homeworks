import React, {Component} from 'react';
import Modal from "./Modal/Modal";
import Button from "./Button/Button";
import "./App.css"

class App extends Component {
    state = {
        modal: null
    }
    render() {
        return (
            <div className="App">
                <div className="App-button-wrapper">
                    <Button onClick={() => {
                        this.setState({
                            modal: <Modal header="Do you want to delete this file?"
                                          closeButton={true}
                                          text="Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?"
                                          bgColorWrapper="#E74C3C"
                                          bgColorHeader="#D44637"
                                          btnModCol="#D44637"
                                          closeModal={ () => this.setState({
                                              modal: null
                                          }) }
                            />
                        })

                    }}
                            text="Open first modal"
                            backgroundColor="green"/>
                    <Button onClick={() => {
                        this.setState({
                            modal: <Modal header="Do you want to save this file?"
                                          closeButton={false}
                                          text="Это заставит браузер работать в режиме соответствия стандартам, ваших страниц в разных браузерах."
                                          bgColorWrapper="#399216"
                                          bgColorHeader="#396616"
                                          btnModCol="#396616"
                                          closeModal={ () => this.setState({
                                              modal: null
                                          }) }/>
                        })
                    }}
                            text="Open second modal"
                            backgroundColor="blue"/>
                </div>
                {this.state.modal}
            </div>
        );
    }
}

export default App;
