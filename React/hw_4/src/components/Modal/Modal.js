import React from 'react';
import Button from "../Button/Button";
import "./Modal.scss";


const Modal = ({header, closeButton, text, bgColorWrapper, bgColorHeader, btnModCol, closeModal}) => {
    return (
        <div className="modal-background">
            <div className="modal-wrapper" style={{backgroundColor: bgColorWrapper}}>
                <div className="modal-wrapper__header" style={{backgroundColor: bgColorHeader}}>
                    <span>{header}</span>
                    {closeButton ? <button className="close-btn" onClick={closeModal}>X</button> : null}
                </div>
                <div className="modal-wrapper__main-text">
                    <span>{text}</span>
                </div>
                <div className="modal-wrapper__main">
                    <Button
                        text="OK"
                        backgroundColor={btnModCol}
                        onClick={closeModal}/>
                </div>
            </div>
        </div>
    );
};

export default Modal;
