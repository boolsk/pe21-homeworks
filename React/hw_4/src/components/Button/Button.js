import React from 'react';
import "./Button.scss"


const Button = ({text, backgroundColor, onClick}) => {
    return (
        <button
            onClick={() =>
                onClick()
            }
            className="btn"
            style={{backgroundColor: backgroundColor}}>{text}</button>
    );
};

export default Button;
