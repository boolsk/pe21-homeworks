import React from 'react';
import ProductItem from "../productItem/ProductItem";
import "./ItemList.scss"

const ItemList = ({addToFavorites, favorites, addToCart, boughtProducts, itemsProp}) => {
    const itemComponent = itemsProp.map((item) => {
        return <ProductItem
            prodId={item.id}
            addToFavorites={addToFavorites}
            title={item.title}
            price={item.price}
            pictureUrl={item.pictureUrl}
            key={item.id}
            favorites={favorites}
            addToCart={addToCart}
            cartItem={item}
            boughtProducts={boughtProducts}/>
    })

    return (
        <div className="item-list-wrapper">
            {itemComponent}
        </div>
    );
};

export default ItemList;