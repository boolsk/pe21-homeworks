let bigArr = ['hello', 'world', 23, '23', null, 'bob', 17, 29, '59', 'round', 'ua'];


function filterBy(arr, type){
    let myArray = arr.filter(function (item, index) {
        if (typeof item === type)
            return item ;
    });
    return myArray;
}

let newArray = filterBy(bigArr, "number");
console.log(newArray);
