let letterEnter = document.getElementById("enter");
let letterS = document.getElementById("s");
let letterE = document.getElementById("e");
let letterO = document.getElementById("o");
let letterN = document.getElementById("n");
let letterL = document.getElementById("l");
let letterZ = document.getElementById("z");

function changeBg(event) {
    letterEnter.classList.remove("btn-blue");
    letterS.classList.remove("btn-blue");
    letterE.classList.remove("btn-blue");
    letterO.classList.remove("btn-blue");
    letterN.classList.remove("btn-blue");
    letterL.classList.remove("btn-blue");
    letterZ.classList.remove("btn-blue");

    if (event.key === "Enter") {
        letterEnter.classList.add("btn-blue");
    }
    if (event.key.toLocaleUpperCase() === "S") {
        letterS.classList.add("btn-blue");
    }
    if (event.key.toLocaleUpperCase() === "E") {
        letterE.classList.add("btn-blue");
    }
    if (event.key.toLocaleUpperCase() === "O") {
        letterO.classList.add("btn-blue");
    }
    if (event.key.toLocaleUpperCase() === "N") {
        letterN.classList.add("btn-blue");
    }
    if (event.key.toLocaleUpperCase() === "L") {
        letterL.classList.add("btn-blue");
    }
    if (event.key.toLocaleUpperCase() === "Z") {
        letterZ.classList.add("btn-blue");
    }
    else {
        console.log(event.key);
    }
}
document.addEventListener("keydown", changeBg);
