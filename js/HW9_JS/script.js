
+function () {
    document.querySelector(".tabs-title").classList.add("active");
    document.querySelector(".tab-text").classList.add("active");
    function selectTab(event) {
        console.dir(event.target.dataset.target);
        let target = event.target.dataset.target;
        document.querySelectorAll(".tabs-title").forEach(el => {
            el.classList.remove("active");
            event.target.classList.add("active");
            document.querySelector("."+ target).classList.add("active");
        });

        document.querySelectorAll(".tab-text").forEach(el => {
            el.classList.remove("active");
            event.target.classList.add("active");
            document.querySelector("."+ target).classList.add("active");
        });
    }
    document.querySelectorAll(".tabs-title").forEach(el => {
        el.addEventListener("click", selectTab)
    })

}();
