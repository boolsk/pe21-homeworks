function calculator() {
    if (operator === "+") {
        return number1 + number2;
    } else if (operator === "-") {
        return number1 - number2;
    } else if (operator === "/") {
        return number1 / number2;
    } else if (operator === "*") {
        return number1 * number2;
    }
}
let number1 = +prompt("Введите первое число");
while (isNaN(number1) || number1 == "" || number1 === undefined) {
    number1 = +prompt("Некоректно введеные данные. Введите первое число еще раз");
}
console.log("Ваше первое число " + number1);

let number2 = +prompt("Введите второе число");
while (isNaN(number2) || number2 == "" || number2 === undefined) {
    number2 = +prompt("Некоректно введеные данные. Введите второе число еще раз");
}
console.log("Ваше второе число " + number2);

let operator = prompt("Для проведения мамематических функций введите следующий символ: ' + ' для суммирования, ' - ' - для вычитания, ' / ' - для деления,  ' * ' - для умножения");
while (operator !== "+" && operator !== "-" && operator !== "/" && operator !== "*") {
    operator = prompt("Некоректно введены данные. Введите один из символов: ' + ' для суммирования, ' - ' - для вычитания, ' / ' - для деления,  ' * ' - для умножения");
}
console.log("Результат операции " + number1 + operator + number2 + " равен " + calculator());
