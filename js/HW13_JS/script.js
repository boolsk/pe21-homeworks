function theme() {
    let btn = document.getElementById("newTheme");
    if (localStorage.getItem("bgBody") !== null && localStorage.getItem("textColor") !== null) {
        let bgColor = localStorage.getItem("bgBody");
        let textColor = localStorage.getItem("textColor");
        document.getElementsByTagName("body")[0].style.backgroundColor = bgColor;
        document.getElementsByTagName("body")[0].style.color = textColor;
    }

    btn.onclick = function () {
        document.getElementsByTagName("body")[0].style.backgroundColor = "coral";
        document.getElementsByTagName("body")[0].style.color = "white";
        localStorage.setItem("bgBody", "coral");
        localStorage.setItem("textColor", "white");
    };
}
theme();


function clear() {
    btnClear = document.getElementById("oldTheme");
    btnClear.onclick = function () {
        localStorage.clear();
        location.reload();
    }
}
clear();