Теория:

1. Обьяснить своими словами разницу между обьявлением переменных через var, let и const.

Отличие let от var: Переменная, объявленная через var, видна везде в функции. Переменная, объявленная через let, видна только в рамках фигурных скобок (блока), в котором объявлена.

let и const - почти одно и тоже. Только const нельзя переназначить.
области видимости у них одинаковые, ограничены блоком.

Если var обявлена вне блока функции, то она будет доступна везде.


2. Почему объявлять переменную через var считается плохим тоном?

если var обявили глобально - то это может привести к ряду каких то ошибок. Например несколько програмистов работают над одним кодом и обявили одинаковую переменную, но с разными значениями, и всё, программа работает не правилно.