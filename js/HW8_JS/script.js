let price = document.getElementById("price");
let notCurrentPrice = document.createElement("span");
let spanTop = document.createElement("span");
let btnSpanTop = document.createElement("button");
btnSpanTop.addEventListener("click", () => {
    btnSpanTop.remove();
    spanTop.remove();
    price.value = "";

});

price.onblur = function () {
    if (price.value > 0) {
        document.body.prepend(spanTop);
        spanTop.after(btnSpanTop);
        btnSpanTop.innerHTML = "x";
        spanTop.innerHTML = "Текущая цена:" + price.value;
        notCurrentPrice.innerHTML = '';

    } else if (price.value <= 0) {
        price.style.borderColor = "red";
        document.body.append(notCurrentPrice);
        notCurrentPrice.innerHTML = 'Please enter correct price';
        notCurrentPrice.style.color = "red";
        spanTop.innerHTML = '';
        btnSpanTop.remove();
    }
};
price.onfocus = function () {
    price.style.borderColor = "green";
};