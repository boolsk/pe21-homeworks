function createNewUser() {
    let name = prompt("Enter you Name in English");
    let surname = prompt("Enter you Surname in English");
    let newUser = {
        firstName: name,
        lastName: surname,
        getLogin: function () {
            return this.firstName[0].toLocaleLowerCase() + this.lastName.toLocaleLowerCase();
        }
    };
    return newUser;
}

const user = createNewUser();
console.log(user.getLogin());