function createNewUser() {
    let name = prompt("Enter you Name in English");
    let surname = prompt("Enter you Surname in English");
    let birth = prompt("Enter you date of birthday (format dd.mm.yyyy)");

    let newUser = {
        firstName: name,
        lastName: surname,
        birthday: birth,
        getLogin: function () {
            return this.firstName[0].toLocaleLowerCase() + this.lastName.toLocaleLowerCase();
        },
        getAge: function () {
            let now = new Date();
            let nowToMs = now.getTime();
            let newBirth = birth.split(".")[2] + "." + birth.split(".")[1] + "." + birth.split(".")[0];
            let newBirthToMs = Date.parse(newBirth);
            let getAgeInMs = nowToMs - newBirthToMs;
            let getAgeInYear = (Math.floor(getAgeInMs / 1000 / 60 / 60 / 24 / 365.25));
            return getAgeInYear;
        },
        getPassword: function () {
            return this.firstName[0].toLocaleUpperCase() + this.lastName.toLocaleLowerCase() + birth.split(".")[2];
        },
    };
    return newUser;
}
const user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());

